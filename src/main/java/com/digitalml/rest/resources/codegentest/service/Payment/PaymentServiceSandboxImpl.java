package com.digitalml.rest.resources.codegentest.service.Payment;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;

import com.digitalml.rest.resources.codegentest.service.PaymentService;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang.StringUtils;

/**
 * Sandbox implementation for: Payment
 * Make payments.123
 *
 * @author admin
 * @version 1.0
 *
 */

public class PaymentServiceSandboxImpl extends PaymentService {
	

    public CreatePaymentCurrentStateDTO createpaymentUseCaseStep1(CreatePaymentCurrentStateDTO currentState) {
    

        CreatePaymentReturnStatusDTO returnStatus = new CreatePaymentReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Make payments.123");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreatePaymentCurrentStateDTO createpaymentUseCaseStep2(CreatePaymentCurrentStateDTO currentState) {
    

        CreatePaymentReturnStatusDTO returnStatus = new CreatePaymentReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Make payments.123");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreatePaymentCurrentStateDTO createpaymentUseCaseStep3(CreatePaymentCurrentStateDTO currentState) {
    

        CreatePaymentReturnStatusDTO returnStatus = new CreatePaymentReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Make payments.123");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreatePaymentCurrentStateDTO createpaymentUseCaseStep4(CreatePaymentCurrentStateDTO currentState) {
    

        CreatePaymentReturnStatusDTO returnStatus = new CreatePaymentReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Make payments.123");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreatePaymentCurrentStateDTO createpaymentUseCaseStep5(CreatePaymentCurrentStateDTO currentState) {
    

        CreatePaymentReturnStatusDTO returnStatus = new CreatePaymentReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Make payments.123");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreatePaymentCurrentStateDTO createpaymentUseCaseStep6(CreatePaymentCurrentStateDTO currentState) {
    

        CreatePaymentReturnStatusDTO returnStatus = new CreatePaymentReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Make payments.123");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


	/**
	 * Creates and returns a {@link Method} object using reflection and handles the possible exceptions.
	 * <br/>
	 * Aids with calling the process step method based on the outcome of the control logic
	 * 
	 * @param methodName
	 * @param clazz
	 * @return
	 */
	private Method getMethod(String methodName, Class clazz) {
		Method method = null;
		try {
			method = PaymentService.class.getMethod(methodName, clazz);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		return method;
	}
	
	/**
	 * For use when calling provider systems.
	 * <p>
	 * TODO: Implement security logic here
	 */
	protected SecurityContext securityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}