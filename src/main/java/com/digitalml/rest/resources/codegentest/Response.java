package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Response:
{
  "required": [
    "payment"
  ],
  "type": "object",
  "properties": {
    "payment": {
      "$ref": "Payment"
    }
  }
}
*/

public class Response {

	@Size(max=1)
	@NotNull
	private com.digitalml.www.information.model.retail.Payment payment;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    payment = new com.digitalml.www.information.model.retail.Payment();
	}
	public com.digitalml.www.information.model.retail.Payment getPayment() {
		return payment;
	}
	
	public void setPayment(com.digitalml.www.information.model.retail.Payment payment) {
		this.payment = payment;
	}
}