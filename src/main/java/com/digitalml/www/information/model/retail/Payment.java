package com.digitalml.www.information.model.retail;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Payment:
{
  "type": "object",
  "properties": {
    "expiryDateMonth": {
      "type": "string"
    },
    "expiryDateYear": {
      "type": "string"
    },
    "securityCode": {
      "type": "string"
    }
  }
}
*/

public class Payment {

	@Size(max=1)
	private String expiryDateMonth;

	@Size(max=1)
	private String expiryDateYear;

	@Size(max=1)
	private String securityCode;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    expiryDateMonth = null;
	    expiryDateYear = null;
	    securityCode = null;
	}
	public String getExpiryDateMonth() {
		return expiryDateMonth;
	}
	
	public void setExpiryDateMonth(String expiryDateMonth) {
		this.expiryDateMonth = expiryDateMonth;
	}
	public String getExpiryDateYear() {
		return expiryDateYear;
	}
	
	public void setExpiryDateYear(String expiryDateYear) {
		this.expiryDateYear = expiryDateYear;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
}