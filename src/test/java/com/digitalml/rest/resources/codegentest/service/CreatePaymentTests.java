package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Payment.PaymentServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.PaymentService.CreatePaymentInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.PaymentService.CreatePaymentReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreatePaymentTests {

	@Test
	public void testOperationCreatePaymentBasicMapping()  {
		PaymentServiceDefaultImpl serviceDefaultImpl = new PaymentServiceDefaultImpl();
		CreatePaymentInputParametersDTO inputs = new CreatePaymentInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		CreatePaymentReturnDTO returnValue = serviceDefaultImpl.createpayment(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}